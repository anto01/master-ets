\select@language {french}
\contentsline {chapter}{\numberline {1}\emph {Simulation des r\IeC {\'e}ponses vibro acoustique de substrat de panneaux solaires destin\IeC {\'e}s \IeC {\`a} l'espace}}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}R\IeC {\'e}sum\IeC {\'e}}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Introduction}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Revue de la litt\IeC {\'e}rature}{2}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Mise en situation}{2}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Influence du son sur les structures}{3}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Application aux panneaux solaires}{3}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}M\IeC {\'e}thodologie}{3}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Description de la probl\IeC {\'e}matique}{3}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Objectifs du m\IeC {\'e}moire}{3}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}M\IeC {\'e}thode pour parvenir \IeC {\`a} ces fins}{3}{subsection.1.4.3}
\contentsline {subsubsection}{Outils d'analyses num\IeC {\'e}riques}{3}{subsubsection*.3}
\contentsline {subsection}{\numberline {1.4.4}D\IeC {\'e}marche du projet}{3}{subsection.1.4.4}
\contentsline {section}{\numberline {1.5}Partie exp\IeC {\'e}rimentale}{3}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Montage exp\IeC {\'e}rimentale}{3}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Caract\IeC {\'e}risation du montage}{4}{subsection.1.5.2}
\contentsline {subsubsection}{Caract\IeC {\'e}risation du joint boulonn\IeC {\'e}}{4}{subsubsection*.4}
\contentsline {subsection}{\numberline {1.5.3}Acquisition des donn\IeC {\'e}es}{4}{subsection.1.5.3}
\contentsline {subsection}{\numberline {1.5.4}Extraction et traitement des donn\IeC {\'e}es}{4}{subsection.1.5.4}
\contentsline {section}{\numberline {1.6}Partie num\IeC {\'e}rique}{4}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Mod\IeC {\`e}le dynamique NX-NASTRAN}{4}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Mod\IeC {\`e}le vibroacoustique VA-one}{4}{subsection.1.6.2}
\contentsline {subsection}{\numberline {1.6.3}Mod\IeC {\`e}le vibroacoustique wave-six}{4}{subsection.1.6.3}
\contentsline {section}{\numberline {1.7}Corr\IeC {\'e}lations des mod\IeC {\`e}les}{5}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}Corr\IeC {\'e}lation pour VA-one}{5}{subsection.1.7.1}
\contentsline {subsection}{\numberline {1.7.2}Corr\IeC {\'e}lation pour wave-six}{5}{subsection.1.7.2}
\contentsline {section}{\numberline {1.8}R\IeC {\'e}sultats et interpr\IeC {\'e}tation}{5}{section.1.8}
\contentsline {section}{\numberline {1.9}Discussion}{5}{section.1.9}
\contentsline {section}{\numberline {1.10}Conclusion}{5}{section.1.10}
\contentsline {section}{\numberline {1.11}Recommandations}{5}{section.1.11}
