========================
Corrélations des modèles
========================

* Premièrement expliquer le but de la corrélation.

* Impacte des corrélations sur les résultats et objectifs du mémoire.

* Méthodes utilisées

Corrélation pour VA-one
=======================

* Corrélation initiale des modèles

* Recalage des modèles

* Corrélation finale

* Illustration des résultats (graph 2D, 3D et tableau)


Corrélation pour wave-six
=========================

* Corrélation initiale des modèles

* Recalage des modèles

* Corrélation finale

* Illustration des résultats (graph 2D, 3D et tableau)







