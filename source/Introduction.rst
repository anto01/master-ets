============
Introduction
============

* Introduction de l’environnement ASC/ETS/POLY et une courte explication du projet de développement du panneau avec CAL.


* Description brève des méthodes utilisées par l’industrie pour faire les simulations vibroacoustiques (inclure les méthodes de vibrations mécaniques afin de couvrir les tests acoustiques).


* Énoncer seulement l’objectif principal (grandes lignes du projet).


* L’intérêt ou l’utilité que ça va apporter au domaine. Identifier le fait que c’est surtout une méthode qui est proposée et que plusieurs types de simulations ont été testés (Nastran, Va-one, Wave-six).





