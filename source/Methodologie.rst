============
Méthodologie
============

Description de la problématique
===============================

* Formulation claire et précise de la problématique.



Objectifs du mémoire
====================

* Définir l'objectifs du mémoire et les limitations.


Méthode pour parvenir à ces fins
================================

* Expliquer les outils sélectionné et les raisons (basses fréquences etc.).

* Décrire les étapes qui seront entreprises.


Outils d'analyses numériques
----------------------------

    * Description de NX-NASTRAN et sont utilité pour résoudre le problème.

    * Description de VA-ONE et sont utilité pour résoudre le problème.

    * Description de WAVE-SIX et sont utilité pour résoudre le problème.


Démarche du projet
==================

* Construction d'un schéma bloc et expliquer les étapes.
