====================
Partie expérimentale
====================


Montage expérimentale
=====================

* But des tests (validation des modèles et caractérisation du panneau)

* Explication des tests effectués au CNRC d'Ottawa, identifier les types de tests et les raisons qui ont poussé à faire ces tests.

 

Caractérisation du montage
==========================

* Identifier la problématique des joints coniques.

* Élaborer sur les hypothèses de glissement ou comportement non linéaire du panneau. 

Caractérisation du joint boulonné
---------------------------------

* Inclure tout ce qui a été fait lors de l'article (test, méthode, résultats)



Acquisition des données
=======================

* Pour les tests à Ottawa, expliquer les types de sensors et leurs utilités. Pourquoi ils ont été positionnés de telle façon.

* Utilisation du logiciel LMS Test Lab (Siemens) et les types de fichiers résultants


Extraction et traitement des données
====================================

* Expliquer comment les données sont traitées et comment ceux-ci sont manipulés afin de pouvoir comparer avec les modèles numériques. Discuter des outils utilisés (LMS Test Lab, polymax curve fitting, fichiers unv.)

* Illustrer les résultats sous forme de graphie 2 et 3D (mode shapes, FRF, eig Freq, eig Value)


