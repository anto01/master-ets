====================
Partie numérique
====================


Modèle dynamique NX-NASTRAN
===========================

* Bref explication de NX-NASTRAN

* Expliquer le modèle et son utilité.

* Équations primaire du modèles


Modèle vibroacoustique VA-one
=============================

* Bref explication de VA-one

* Expliquer le modèle et son utilité.

* Équations primaire du modèles

* Résultats des analyses

* Beaucoup de support visuel, graph, mode shapes etc.


Modèle vibroacoustique wave-six
===============================

* Bref explication de Wave-six

* Expliquer le modèle et son utilité.

* Équations primaire du modèles

* Résultats des analyses

* Beaucoup de support visuel, graph, mode shapes etc.





