======
Résumé
======

* La problématique du son sur les panneaux et les difficultés des simulations vibroacoutiques.

* L’objectif principal de simuler et valider les réponses vibroacoustiques d’un panneau.

* La démarche utilisée pour parvenir à faire ces simulations

* Les principaux résultats et l’interprétation de ceux-ci (avec un peu de chiffres)

* L’aspect particulier de la résolution du problème (approche d’ingénieur et validation des résultats)

* Limitation des travaux du mémoire











