=======================
Revue de la littérature
=======================


Mise en situation
====================

* Une bref introduction de l'acoustique en générale.

* Les théories existantes sur l'excitation acoustique.

* L’acoustique des lanceurs, comment le son est produit et pourquoi c’est dangereux.

* Les règles à respecter provenant de la compagnie du lanceur (manuel d'utilisateur).



Influence du son sur les structures
======================================

* L’aspect de ce type d’excitation sur les structures (excitation aléatoire, vibroacoustique).

* Danger d’atteindre des modes importants et citer des exemples.

* Quelles théories ont été développées pour analyser les structures (éléments finis, éléments de frontières, analyses statistique).



Application aux panneaux solaires
====================================

* Élaborer sur le fait que les panneaux solaires sont facilement excitables au niveau acoustique.

* Ce sont des structures avec une grande surface et grande rigidité et qu'est-ce que ça engendre.









